$(document).ready(function() {
    $('.carousel-control').click(function (e) {
    	$('.item.active').find('.carousel-caption').fadeOut();
    	$('.item').find('.carousel-caption').fadeIn();
    });
});